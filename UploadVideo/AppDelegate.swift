//
//  AppDelegate.swift
//  UploadVideo
//
//  Created by Vitaliy Obertynskyi on 3/15/17.
//  Copyright © 2017 Vitaliy Obertynskyi. All rights reserved.
//

import UIKit
import Google
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signInSilently()
        GIDSignIn.sharedInstance().scopes = [
            "https://www.googleapis.com/auth/youtube",
            "https://www.googleapis.com/auth/plus.login"
        ]
        
        return true
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let source = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String
        let annotation  = options[UIApplicationOpenURLOptionsKey.annotation] as Any
        
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: source, annotation: annotation)
    }
}

extension AppDelegate: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            // Perform any operations on signed in user here.
            
            let username = user.profile.name
            let useremail = user.profile.email
            let token = user.authentication.accessToken
            
            print("user: \(username) email: \(useremail) >>> \ntoken: \(token)")
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
}

extension AppDelegate: GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        
        self.window?.rootViewController?.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
        viewController.dismiss(animated: true, completion: nil)
    }
}
