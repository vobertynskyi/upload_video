//
//  ViewController.swift
//  UploadVideo
//
//  Created by Vitaliy Obertynskyi on 3/15/17.
//  Copyright © 2017 Vitaliy Obertynskyi. All rights reserved.
//

import UIKit
import Foundation

import Google
import GoogleSignIn

import Alamofire
import MobileCoreServices


class ViewController: UIViewController {
    
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    // MARK: - Action
    @IBAction private func signoutTap(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
    }
    
    @IBAction private func uploadVideo(_ sender: UIButton) {
        
        if GIDSignIn.sharedInstance().currentUser == nil {
            print("Error: User no found")
            return
        }
        
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        picker.mediaTypes = [kUTTypeMovie as String]
        
        self.present(picker, animated: true, completion: nil)
    }
    
    func postVideoToYouTube(token: String, filePath: String, callback: @escaping (Bool) -> Void){
        
        let headers = ["Authorization": "Bearer \(token)"]
        
        guard let rURL = URL(string: "https://www.googleapis.com/upload/youtube/v3/videos?part=id") else {
            print("Error file url")
            return
        }
        
        guard let fileName = URL(string: filePath)?.standardizedFileURL.lastPathComponent else {
            print("Error file name")
            return
        }
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(URL(string: filePath)!,
                                         withName: "binaryContent",
                                         fileName: fileName,
                                         mimeType: "video/*, application/octet-stream")
        },
            to: rURL,
            headers: headers,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("response is ", response)
                        
                        if((response.result.value) != nil) {
                            print("respnse date", response)
                        }
                        }
                        
                        .uploadProgress { progress in // main queue by default
                            
                            print("Upload Progress: \(progress.fractionCompleted * 100)")
                    }
                    
                case .failure(let encodingError):
                    print("failed", errno)
                    print("ERROR RESPONSE: \(encodingError)")
                    break
                }
        })
    }
    
    private func precessResult(result: SessionManager.MultipartFormDataEncodingResult) {
        
        switch result {
            
        case .success(let upload, _, _):
            
            upload.responseJSON { response in
                
                debugPrint(response)
                print(response.result.value ?? "nil")
            }
        case .failure(let encodingError):
            
            print(encodingError)
        }
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let videoFileURL = info[UIImagePickerControllerMediaURL] as? URL {
            
            print("File URL: \(videoFileURL)")
            
            postVideoToYouTube(token: GIDSignIn.sharedInstance().currentUser.authentication.accessToken,
                               filePath: videoFileURL.absoluteString, callback: { (flag) in
                                
                                print("")
            })
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
