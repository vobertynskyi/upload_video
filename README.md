# Upload view to Youtube
Fistful we should know and remember about pre-setup this process on the google console (console.developers.google.com/apis/dashboard)
After enabling youtube API you should create OAuth 2.0 client IDs for the iOS application. May download .plist file with configuration data for your the application and move it to the project. 
Create podfile after you created the project. Copy/paste next line: 

    platform :ios, ‘9.0’
    target 'YOUR_APP_TARGET_NAME' do
    use_frameworks!
	    pod 'Google/SignIn'
	    pod 'Alamofire'
    end

Open workspace. Open AppDelegate file and copy/paste next line to method 


```
#!swift

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
```
for setup google lib:

        
```
#!swift

// Initialize sign-in
        var configureError: NSError? GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signInSilently()
        GIDSignIn.sharedInstance().scopes = [
            "https://www.googleapis.com/auth/youtube",
            "https://www.googleapis.com/auth/plus.login"
        ]
```


This code will do authentication for user and support upload video. 

Ok. A few steps and we will finish setup. Next a func handling deep link for authentication, copy/paste again:


```
#!swift

func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let source = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String
        let annotation  = options[UIApplicationOpenURLOptionsKey.annotation] as Any
        
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: source, annotation: annotation)
}
```

    
And last one extension for AppDelegate:
    
    
```
#!swift

extension AppDelegate: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            // Perform any operations on signed in user here.
            
            let username = user.profile.name
            let useremail = user.profile.email
            let token = user.authentication.accessToken
            
            print("user: \(username) email: \(useremail) >>> \ntoken: \(token)")
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
}
```
	
```
#!swift

extension AppDelegate: GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        
        self.window?.rootViewController?.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
        viewController.dismiss(animated: true, completion: nil)
    }
}
```